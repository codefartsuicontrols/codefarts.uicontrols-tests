﻿
namespace Codefarts.UIControls_Tests
{
    using System.Xml;

    using Codefarts.UIControls;
    using Codefarts.UIControls.Converter;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class XMLParserTests
    {
        [TestMethod]
        public void Build_With_Null_Node()
        {
            var parser = new XMLParser<Control>();
            try
            {
                var result = parser.Build(null, null);
                Assert.Fail("Result should be null.");
            }
            catch (System.Exception ex)
            {
                // success if exception thrown
            }
        }

        [TestMethod]
        public void Build_With_Test_Node_And_No_Plugins()
        {
            var parser = new XMLParser<Control>();
            var doc = new XmlDocument();
            try
            {
                var result = parser.Build(null, doc.CreateNode(XmlNodeType.Element, "test", null));
                Assert.Fail("Should have thrown an exception.");
            }
            catch (System.Exception ex)
            {
                // success if exception thrown
            }
        }
    }
}
