﻿namespace Codefarts.Tests.UIControls.Converters
{
    using System;
    using System.Xml;

    using Codefarts.UIControls;
    using Codefarts.UIControls.Converter;
    using Codefarts.UIControls.Converter.Parsers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class UserControlParserTests
    {
        private UserControlParser parser;

        private XMLParser<Control> xamlParser;

        private XmlDocument document;

        [TestInitialize]
        public void Setup()
        {
            this.parser = new UserControlParser();
            this.xamlParser = new XMLParser<Control>();
            this.xamlParser.Add(this.parser);

            this.document = this.CreateUserControlTestDocument();
        }

        private XmlDocument CreateUserControlTestDocument()
        {
            var doc = new XmlDocument();

            var xmlData = "<UserControl x:Class=\"TestClasses.UserControlTestClass\"\r\n"
                          + "xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"\r\n"
                          + "xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"\r\n"
                          + "xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\"\r\n"
                          + "xmlns:d=\"http://schemas.microsoft.com/expression/blend/2008\"\r\n" + "mc:Ignorable=\"d\"\r\n"
                          + "d:DesignHeight=\"300\" d:DesignWidth=\"300\">\r\n" +
                // "    <Grid>\r\n" +
                // "        <Button Content=\"Button\" HorizontalAlignment=\"Left\" Margin=\"10,10,0,0\" VerticalAlignment=\"Top\" Width=\"75\"/>\r\n" +
                // "        <CheckBox Content=\"CheckBox\" HorizontalAlignment=\"Left\" Margin=\"90,13,0,0\" VerticalAlignment=\"Top\"/>" +
                // "    </Grid>\r\n" +
                          "</UserControl>";

            doc.LoadXml(xmlData);
            return doc;
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.parser = null;
        }

        [TestMethod]
        public void NonUserControlElement()
        {
            try
            {
                var result = this.xamlParser.Build(null, this.document.DocumentElement);
                Assert.IsNotNull(result, "xamlParser result was null!");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}