﻿namespace Codefarts.Tests.UIControls.Converters
{
    using System;
    using System.Xml;

    using Codefarts.UIControls;
    using Codefarts.UIControls.Converter;
    using Codefarts.UIControls.Converter.Parsers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ButtonControlParserTests
    {
        private ButtonParser parser;

        private XMLParser<Control> xamlParser;

        private XmlDocument document;

        [TestInitialize]
        public void Setup()
        {
            this.parser = new ButtonParser();
            this.xamlParser = new XMLParser<Control>();
            this.xamlParser.Add(this.parser);

            this.document = this.CreateButtonTestDocument();
        }

        private XmlDocument CreateButtonTestDocument()
        {
            var doc = new XmlDocument();

            var xmlData = "<Button Content=\"Button\" HorizontalAlignment=\"Left\" Margin=\"10,10,0,0\" VerticalAlignment=\"Top\" Width=\"75\"/>\r\n";

            doc.LoadXml(xmlData);
            return doc;
        }

        [TestCleanup]
        public void Cleanup()
        {
            this.parser = null;
        }

        [TestMethod]
        public void ButtonElement()
        {
            try
            {
                var result = this.xamlParser.Build(null, this.document.DocumentElement);
                Assert.IsInstanceOfType(result, typeof(Button));
                var button = result as Button;
                Assert.IsNotNull(result, "xamlParser result was null!");
                Assert.IsTrue(result.IsEnabled, "Is not enabled");
                Assert.AreEqual(result.Visibility, Visibility.Visible, "Visibility not Visible");
                Assert.AreEqual(button.Text, "Button", "Text is not 'Button'.");
                Assert.AreEqual(button.HorizontalAlignment, HorizontalAlignment.Left, "Horizontal alignment is not set to left.");
                Assert.AreEqual(button.VerticalAlignment, VerticalAlignment.Top, "Vertical alignment is not set to top.");
                Assert.AreEqual(button.Width, 75, "Width is not set to 75.");
                Assert.AreEqual(button.Height, 0, "Height is not set to 0.");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}